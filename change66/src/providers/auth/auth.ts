import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

let apiUrl = "http://wisehouse.no/internett1/";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(public http: Http) {
    console.log('Hello AuthProvider Provider');
  }

  postLogin(credentials, type){
    return this.http.post(apiUrl+type, JSON.stringify(credentials))
    .map(response => response.json());
  }

  postData(credentials, type)
  {

    return new Promise((resolve, reject) => {
    let headers = new Headers();

    this.http.post(apiUrl+type, JSON.stringify(credentials), {headers: headers}).subscribe(res => {
        resolve(res.json());
    }, (err) => {
      reject(err);
    });

   });

  }

}
