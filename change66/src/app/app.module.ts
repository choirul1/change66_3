import { BrowserModule } from '@angular/platform-browser';

//import { Pro } from '@ionic/pro';

import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EditProfilePage } from '../pages/editprofile/editprofile';
import { MainPage } from '../pages/main/main';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ModalpagePage } from '../pages/modalpage/modalpage';
import { ModalcompanyPage } from '../pages/modalcompany/modalcompany';
import { EditpicturePage } from '../pages/editpicture/editpicture';
import { MynotesPage } from '../pages/mynotes/mynotes';
import { NoteconfigPage } from '../pages/noteconfig/noteconfig';
import { MyscoresPage } from '../pages/myscores/myscores';

//import { OneSignal } from '@ionic-native/onesignal';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EditProfilePage,
    EditpicturePage,
    SignupPage,
    MainPage,
    LoginPage,
    MyscoresPage,
    MynotesPage,
    NoteconfigPage,
    ModalpagePage,
    ModalcompanyPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EditProfilePage,
    EditpicturePage,
    SignupPage,
    MainPage,
    LoginPage,
    MyscoresPage,
    MynotesPage,
    NoteconfigPage,
    ModalpagePage,
    ModalcompanyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //OneSignal,
    IonicErrorHandler,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    File,
    FilePath,
    FileTransferObject,
    FileTransfer,
    Camera
  ]
})
export class AppModule {}
