import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { OneSignal } from '@ionic-native/onesignal';
import { HomePage } from '../pages/home/home';
// import { MyprofilePage } from '../pages/myprofile/myprofile';
import { MynotesPage } from '../pages/mynotes/mynotes';
import { MyscoresPage } from '../pages/myscores/myscores';
import { MainPage } from '../pages/main/main';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MainPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, ) {
    this.initializeApp();
    // this.inActivemenu();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'My Profile', component: HomePage },
      { title: 'My Notes', component: MynotesPage },
      { title: 'My scores', component: MyscoresPage }
    ];

    let CekLogin = JSON.parse(localStorage.getItem('userData'));
    if(CekLogin)
    {
      if(CekLogin.userData.PersonID > 0)
      {
        this.rootPage = HomePage;
      }
    }

    // this.oneSignal.startInit('041324ac-2c63-4996-93e2-9873677ec76e', '1071372104824');
    //
    // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    //
    // this.oneSignal.handleNotificationReceived().subscribe(() => {
    //  // do something when notification is received
    // });
    //
    // this.oneSignal.handleNotificationOpened().subscribe(() => {
    //   // do something when a notification is opened
    // });
    //
    // this.oneSignal.getIds(); {
    //   alert(this.oneSignal.getIds);
    // }
    //
    //
    // this.oneSignal.endInit();

    // var notificationOpenedCallback = function(jsonData) {
    //   alert('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    // };
    //
    // window["plugins"].OneSignal
    //   .startInit("041324ac-2c63-4996-93e2-9873677ec76e", "1071372104824")
    //   .handleNotificationOpened(notificationOpenedCallback)
    //   .endInit();
   }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    if (this.platform.is('cordova'))
    {
        // this._OneSignal.startInit("041324ac-2c63-4996-93e2-9873677ec76e", "1071372104824");
        // this._OneSignal.inFocusDisplaying(this._OneSignal.OSInFocusDisplayOption.Notification);
        // this._OneSignal.setSubscription(true);
        // this._OneSignal.handleNotificationReceived().subscribe(() => {
        //   // handle received here how you wish.
        // });
        // this._OneSignal.handleNotificationOpened().subscribe(() => {
        //   // handle opened here how you wish.
        // });
        // this._OneSignal.endInit();

        //var notificationOpenedCallback = function(jsonData) {
        //alert('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        //};

        //window["plugins"].OneSignal
          //.startInit("041324ac-2c63-4996-93e2-9873677ec76e", "1071372104824")
          //.handleNotificationOpened(notificationOpenedCallback)
          //.endInit();
        //
        //   let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        //
        //   let userID = status.subscriptionStatus.userId
        //       alert("userID = \(userID)");
        //   let pushToken = status.subscriptionStatus.pushToken
        //       alert("pushToken = \(pushToken)");
        //
        //   if pushToken != nil {
        //       if let playerID = userID {
        //           // do something
        //       }
        //   }
        //
        // window.plugins.OneSignal.getPermissionSubscriptionState(function(status) {
        // status.permissionStatus.hasPrompted;
        // status.permissionStatus.status;
        //
        // status.subscriptionStatus.subscribed;
        // status.subscriptionStatus.userSubscriptionSetting;
        // status.subscriptionStatus.userId;
        // status.subscriptionStatus.pushToken;
        // });
    } else {
      // You're testing in browser, do nothing or mock the plugins' behaviour.
      //
      // var url: string = 'assets/mock-images/image.jpg';
    }


  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  backToWelcome(){
     this.nav.setRoot(MainPage);
  }
  logout(){
       localStorage.clear();
       setTimeout(() => this.backToWelcome(), 1000);
  }
  // inActivemenu(){
  //   this.menu.enable(true, 'mainmenu2');
  // }
}
