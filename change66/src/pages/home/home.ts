import { Component } from '@angular/core';
import { EditProfilePage } from '../editprofile/editprofile';
import { NavController, NavParams, App, Platform, MenuController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  random : any;

  items = {"PersonID":"", "FirstName":"", "LastName":"", "Email":"", "CompanyName":"", "Phone":"", "Gender":""};
  //user : Array<any>;
  //userData : any;
  //user = {};
  userSearch = {"email":"","authenProfile":"1"};
  responseData : any;
  constructor(public navCtrl: NavController,
              public app: App,
              public platform: Platform,
              public authProvider: AuthProvider,
              public navParams: NavParams,
              public menu: MenuController) {
              this.inActivemenu();
  }

  ionViewDidLoad()
  {
    let user = JSON.parse(localStorage.getItem('userData'));
    console.log(user);
    if (user){
      this.userSearch.email = user.userData.Email;
    }

    this.authProvider.postData(this.userSearch, 'cekperson.php')
    .then((result) =>
    {
      this.responseData = result;

      this.items = this.responseData;
      console.log(this.items);
      this.random = 'http://wisehouse.no/internett1/filefoto/'+this.items.PersonID+'/Profile.jpg?random=' + Math.random();
    });
  }
  inActivemenu(){
    this.menu.enable(true, 'mainmenu1');
  }
  editProfile(){
    this.navCtrl.push(EditProfilePage);
  }
}

  // backToWelcome(){
  //    this.navCtrl.setRoot(MainPage);
  // }
  // logout(){
  //      localStorage.clear();
  //      setTimeout(() => this.backToWelcome(), 1000);
  // }
