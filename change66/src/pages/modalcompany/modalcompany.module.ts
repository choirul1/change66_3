import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalcompanyPage } from './modalcompany';

@NgModule({
  declarations: [
    ModalcompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalcompanyPage),
  ],
})
export class ModalcompanyPageModule {}
