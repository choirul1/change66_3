import { Component } from '@angular/core';
import { ViewController, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';



@IonicPage()
@Component({
  selector: 'page-modalcompany',
  templateUrl: 'modalcompany.html',
})
export class ModalcompanyPage {
  item = {"CompanyName" : "", "CompanyID" : ""};
  userData = {list:1};
  items: Array<any>;
  itemsStatis: Array<any>;
  responseData : any;

  constructor(
              public loadingCtrl: LoadingController,
              public authProvider: AuthProvider,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.authProvider.postData(this.userData, 'companylist.php')
    .then(
                (result) => {
                  this.responseData = result;

                  this.items = this.responseData;
                  this.itemsStatis = this.responseData;
                }
              );
  }

  setItems(ev: any){
      let val = ev.target.value;

      if (val && val.trim() !== '') {
        this.items = this.itemsStatis.filter(function(item) {
          return item.CompanyName.toLowerCase().includes(val.toLowerCase());
        });
      }
  }

  dismiss(item) {
    console.log(item);
    this.viewCtrl.dismiss(item);
  }

  companyClick(item){
    this.viewCtrl.dismiss(item);
  }

}
