import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { EditpicturePage } from '../editpicture/editpicture';
import { ModalcompanyPage } from '../modalcompany/modalcompany';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditProfilePage {
  //edit = {"firstname":"", "lastname":""};
  reg = {"company":"", "companyid":""};
  update = {"FirstName":"", "LastName":"", "Email":"", "CompanyID":"", "CompanyName":"", "Phone":"", "Gender":"", "authenProfile":""};
  //profile = {"FirstName":"", "LastName":"", "Email":"", "CompanyName":"", "Phone":"", "Gender":""};
  userSearch = {"email":"","authenProfile":"1"};
  responseData : any;
  constructor(public navCtrl: NavController,
              public authProvider: AuthProvider,
              public navParams: NavParams,
              public modalCtrl: ModalController) {}

  ionViewDidLoad() {
    //console.log('ionViewDidLoad EditprofilePage');
    let user = JSON.parse(localStorage.getItem('userData'));
    //console.log(this.user.userData.Email);
    if (user){
      this.userSearch.email = user.userData.Email;
      this.update.Email = this.userSearch.email;
    }

    this.authProvider.postData(this.userSearch, 'cekperson.php')
    .then((result) =>
    {
      this.responseData = result;

      this.update = this.responseData;
      this.update.authenProfile = "1";
      //console.log(this.profile);
    });
  }

  presentCompanyModal() {
   let companyModal = this.modalCtrl.create(ModalcompanyPage);
   companyModal.onDidDismiss(data => {
     //console.log(data);
     this.reg.company = data.CompanyName;
     this.reg.companyid = data.CompanyID;
     this.update.CompanyName = this.reg.company;
     this.update.CompanyID = this.reg.companyid;
     alert(this.update.CompanyID);
   });
   companyModal.present();
 }

 saveProfile(){
   //console.log(this.update);
   if(this.update.FirstName && this.update.LastName && this.update.Email && this.update.authenProfile &&
      this.update.CompanyID && this.update.Phone && this.update.Gender)
   {
    this.authProvider.postData(this.update, "index.php?t=publish.dataconfigimam").then((result) =>{
    this.responseData = result;
    console.log(this.responseData);
    //localStorage.setItem('profile', JSON.stringify(this.resposeData) )
    //alert("Register Success");
    this.navCtrl.setRoot(HomePage);
    }, (err) => {
      console.log(err);
    });
   }
   else{
    alert("Error");
   }
}

editPhoto(){
  this.navCtrl.push(EditpicturePage);
}

}
