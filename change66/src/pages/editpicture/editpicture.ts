import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
//import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { HomePage } from '../home/home';
import { DomSanitizer } from '@angular/platform-browser';


declare var cordova: any;
/**
 * Generated class for the EditpicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editpicture',
  templateUrl: 'editpicture.html',
})
export class EditpicturePage {
  lastImage: any = null;
	loading: Loading;
	userdata = {};
 
	
imageFileName:any;
  constructor(	public navCtrl: NavController, 
								public navParams: NavParams,
								private camera: Camera,
								private file: File,
								private transfer: FileTransfer,
								private filePath: FilePath, 
								public actionSheetCtrl: ActionSheetController, 
								public toastCtrl: ToastController, 
								public platform: Platform, 
								public loadingCtrl: LoadingController,
								private DomSanitizer: DomSanitizer ) {
  }

  ionViewDidLoad() {
		//console.log('ionViewDidLoad EditpicturePage');
		let user = JSON.parse(localStorage.getItem('userData'));
		this.userdata = user.PersonID;
		console.log(this.userdata);
  }

	public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
	}
	
  // takePhoto(){
  // 	const options: CameraOptions = {
	// 		quality: 100,
	// 		destinationType: this.camera.DestinationType.DATA_URL,
	// 		encodingType: this.camera.EncodingType.JPEG,
	// 		mediaType: this.camera.MediaType.PICTURE
	// 	}

	// 	this.camera.getPicture(options).then((imageData) => {
	// 	// imageData is either a base64 encoded string or a file URI
	// 	// If it's base64:
	// 	let base64Image = 'data:image/jpeg;base64,' + imageData;
	// 	}, (err) => {
	// 	// Handle error
	// 	});
  // }

	// public takePicture(sourceType) {
	// 	// Create options for the Camera Dialog
	// 	var options = {
	// 		quality: 100,
	// 		sourceType: sourceType,
	// 		saveToPhotoAlbum: false,
	// 		correctOrientation: true
	// 	};
	 
	// 	// Get the data of an image
	// 	this.camera.getPicture(options).then((imagePath) => {
	// 		// Special handling for Android library
	// 		if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
	// 			this.filePath.resolveNativePath(imagePath)
	// 				.then(filePath => {
	// 					let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
	// 					let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
	// 					this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
	// 				});
	// 		} else {
	// 			let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
	// 			let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
	// 			this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
	// 		}
	// 	}, (err) => {
	// 		this.presentToast('Error while selecting image.');
	// 	});
	// }

	// private createFileName() {
	// 	var d = new Date(),
	// 	n = d.getTime(),
	// 	newFileName =  n + ".jpg";
	// 	return newFileName;
	// }
	 
	// // Copy the image to a local folder
	// private copyFileToLocalDir(namePath, currentName, newFileName) {
	// 	this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
	// 		this.lastImage = newFileName;
	// 	}, error => {
	// 		this.presentToast('Error while storing file.');
	// 	});
	// }
	 
	// private presentToast(text) {
	// 	let toast = this.toastCtrl.create({
	// 		message: text,
	// 		duration: 3000,
	// 		position: 'top'
	// 	});
	// 	toast.present();
	// }


	/////////////////////////////////////////////////////////////////

	public takePicture(sourceType){
    console.log('take picture');
    const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options)
     .then((imageData)=>{
       console.log('end take picture');
				this.lastImage = imageData.replace("data:image/jpeg;base64," + imageData);
      })
      .catch(err=>{
        console.log(err);
        alert(err);
      })
  }
	 
	// Always get the accurate path to your apps folder
	// public pathForImage(lastImage) {
	// 	if (lastImage === null) {
	// 		return '';
	// 	} else {
	// 		return cordova.file.dataDirectory + lastImage;
	// 	}
	// }

	// private createFileName() {
	// 	var d = new Date(),
	// 	n = d.getTime(),
	// 	newFileName =  n + ".jpg";
	// 	return newFileName;
	// }
	 
	// // Copy the image to a local folder
	// private copyFileToLocalDir(namePath, currentName, newFileName) {
	// 	this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
	// 		this.lastImage = newFileName;
	// 	}, error => {
	// 		this.presentToast('Error while storing file.');
	// 	});
	// }
	 

	 
	// // Always get the accurate path to your apps folder
	// public pathForImage(img) {
	// 	if (img === null) {
	// 		return '';
	// 	} else {
	// 		return cordova.file.dataDirectory + img;
	// 	}
	// }


	// uploadFile() {

	// 	var url = "http://wisehouse.no/internett1/index.php?t=publish.parshingfoto&foto=1&iduser="+this.userdata;
	// 	let loader = this.loadingCtrl.create({
	// 		content: "Uploading..."
	// 	});
	// 	loader.present();
	// 	const fileTransfer: FileTransferObject = this.transfer.create();
	
	// 	let options: FileUploadOptions = {
	// 		fileKey: 'ionicfile',
	// 		fileName: 'ionicfile',
	// 		chunkedMode: false,
	// 		mimeType: "image/jpg",
	// 		headers: {}
	// 	}
	
	// 	fileTransfer.upload(this.lastImage, url, options)
	// 		.then((data) => {
	// 		console.log(data+" Uploaded Successfully");
	// 		alert("sukses");
	// 		loader.dismiss();
	// 		this.presentToast("Image uploaded successfully");
	// 		this.navCtrl.setRoot(HomePage);
	// 	}, (err) => {
	// 		console.log(err);
	// 		alert("gagal");
	// 		loader.dismiss();
	// 		this.presentToast(err);
	// 	});
	// }

	
	public uploadImage() {
		// Destination URL
	  var url = "http://192.168.1.3/push/upload.php";
		//var url = "http://wisehouse.no/internett1/index.php?t=publish.parshingfoto&foto=1&iduser="+this.userdata;
		
		// File for Upload
		//var targetPath = this.pathForImage(this.lastImage);
	 
		// File name only
		var filename = this.lastImage;
	 
		var options = {
			fileKey: "upload_data", 
			fileName: filename,
			chunkedMode: false,
			mimeType: "image/jpg",
			params: { 'suppplier_id': "538", 'doc_name': "pan_card"}
		};
	 
		const fileTransfer: FileTransferObject = this.transfer.create();
	 
		this.loading = this.loadingCtrl.create({
			content: 'Uploading...',
		});
		this.loading.present();
	 
		// Use the FileTransfer to upload the image
		fileTransfer.upload(this.lastImage, url, options).then(data => {
			this.loading.dismissAll()
			this.presentToast('Image succesful uploaded.');
			this.navCtrl.setRoot(HomePage);
			alert("sukses");
		}, err => {
			this.loading.dismissAll()
			this.presentToast('Error while uploading file.');
			alert("gagal");
		});
	}

	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 3000,
			position: 'top'
		});
		toast.present();
	}
}
