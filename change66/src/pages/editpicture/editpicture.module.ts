import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditpicturePage } from './editpicture';

@NgModule({
  declarations: [
    EditpicturePage,
  ],
  imports: [
    IonicPageModule.forChild(EditpicturePage),
  ],
})
export class EditpicturePageModule {}
