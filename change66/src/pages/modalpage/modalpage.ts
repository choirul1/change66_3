import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modalpage',
  templateUrl: 'modalpage.html',
})
export class ModalpagePage {
  myParam: string;
   items: Array<any>;
   item = {"kategori" : "", "role" : ""};
  constructor(public viewCtrl: ViewController,
    params: NavParams) {
       this.myParam = params.get('myParam');
  }

  dismiss(item) {
    //alert("tes");
    //this.item = {"kategori" : "", "role" : ""};
    console.log(item);
    this.viewCtrl.dismiss(item);
  }
  ngOnInit() {
   this.setItems();
 }
 setItems() {
   this.items = [
     { kategori : "Employee", role : "100"},
     { kategori : "Leader", role : "1052"},
     { kategori : "Union Representatives", role : "1053"},
     { kategori : "Board Leder Report", role : "1054"},
   ];
 }
 filterItems(ev: any) {
     this.setItems();
     let val = ev.target.value;

     if (val && val.trim() !== '') {
       this.items = this.items.filter(function(item) {
         return item.kategori.toLowerCase().includes(val.toLowerCase());
       });
     }
   }

 catagori(item){
   //alert(JSON.stringify(item));
   this.viewCtrl.dismiss(item);
 }

}
