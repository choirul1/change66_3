import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { MynotesPage } from '../mynotes/mynotes';
/**
 * Generated class for the NoteconfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noteconfig',
  templateUrl: 'noteconfig.html',
})
export class NoteconfigPage {
//myParam = [];
dataSch = {"Schedule":"", "Week":"", "TimeMinute":"", "TimeHour":""};
items = {"Schedule":"", "Week":"", "TimeMinute":"", "TimeHour":""};
myDate: string;
hh: string;
mm: string;
sideList= [];
weekList= [];
responseData : any;
indata = {"email":"", "id":"", "mobile":2 };
clientSideList = [];
clientweekList = [];
schModel :any;
schWeek:any;
schDate:string;
clickupdate = 0;
insert = {"email":"", "id":"", "sch":"", "week":"", "hourtime":"", "minutetime":"","token":"123", "mobile":1 };
update = {"email":"", "id":"", "sch":"", "week":"", "hourtime":"", "minutetime":"","token":"", "mobile":3 };
//update = {"email":"", "id":"", "Schedule":"", "Week":"", "TimeHour":"", "TimeMinute":"","token":"", "mobile":3 }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public authProvider: AuthProvider) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad NoteconfigPage');
    //this.myDate = "00:00";
    this.setclientSideList();
    this.setclientweekList();
    this.indata.id = this.navParams.get('myParam');
    this.insert.id = this.indata.id;


    //get data
    let user = JSON.parse(localStorage.getItem('userData'));

    this.indata.email = user.userData.Email;
    this.insert.email = this.indata.email;
    this.update.email = this.indata.email;

    console.log(this.indata);


    this.authProvider.postData(this.indata, 'index.php?t=publish.dataconfig')
    .then((result) =>
    {
      this.responseData = result;
      console.log(this.responseData);

      if(this.responseData.data != null){
        this.update.id = this.responseData.data.ID;
        this.update.token = this.responseData.data.token;
        //console.log(this.responseData.data);
        //alert(JSON.stringify(this.insert));
        this.items = this.responseData.data;
        if(this.items != null)
        {
          this.schWeek = this.items.Week;
          this.schModel = this.items.Schedule;
          this.schDate = this.items.TimeHour+":"+this.items.TimeMinute;

          this.update.sch = this.items.Schedule;
          this.update.week = this.items.Week;

          if(this.items.Schedule == "j")
          {
            this.sideList = this.clientSideList[0].text;
          }
          else if(this.items.Schedule == "p")
          {
            this.sideList = this.clientSideList[1].text;
          }
          else{
            alert("data masih blm ada harus di insert");
          }

          if(this.items.Week == "10")
          {
            this.weekList = this.clientweekList[0].text;
          }
          else if(this.items.Week == "15")
          {
            this.weekList = this.clientweekList[1].text;
          }
          else
          {
            this.weekList = this.clientweekList[2].text;
          }

          if(this.items.TimeHour.length == 1)
          {
            this.items.TimeHour = "0"+this.items.TimeHour
          }

          if(this.items.TimeMinute.length == 1)
          {
            this.items.TimeMinute = "0"+this.items.TimeMinute
          }

          this.myDate = this.items.TimeHour+":"+this.items.TimeMinute;
          this.clickupdate = 1;
        }
        // else{
        //   this.insert.id = this.indata.id;
        //   this.insert.token = this.responseData.data.token;
        //   //alert("data masih blm ada harus di insert");
        // }
      }


    });
  }

  setclientSideList()
  {
      this.clientSideList = [
        { text: "Job", value: "j" },
        { text: "Privat", value: "p" }
      ];
  }

  setclientweekList()
  {
      this.clientweekList = [
        { text: "10 Week", value: "10" },
        { text: "15 Week", value: "15" },
        { text: "20 Week", value: "20" }
      ];
  }

  setSch(schVal)
  {
    //alert(schVal);
    this.schModel = schVal;
    this.insert.sch = this.schModel;
    this.update.sch = this.schModel;
  }

  setWeek(weekVal)
  {
    //alert(weekVal);
    this.schWeek = weekVal;
    this.insert.week = this.schWeek;
    this.update.week = this.schWeek;
  }

  saveNote()
  {
    this.hh = this.myDate.split(":")[0];
    this.mm = this.myDate.split(":")[1];
    this.insert.hourtime = this.hh;
    this.insert.minutetime = this.mm;
    this.update.hourtime = this.hh;
    this.update.minutetime = this.mm;
    //alert("click");
    let loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });
    loading.present();

    if(this.clickupdate == 0)
    {
      console.log(this.insert);
      if (this.insert.email && this.insert.id && this.insert.sch && this.insert.week && this.insert.hourtime && this.insert.minutetime && this.insert.token && this.insert.mobile)
      {
       console.log(this.insert);
       this.authProvider.postData(this.insert, "index.php?t=publish.dataconfig").then((result) =>{
       //loading.dismiss();
       this.responseData = result;
       console.log(this.responseData);
       loading.dismiss();
       this.navCtrl.setRoot(MynotesPage);
       //alert("insert");
       //localStorage.setItem('detail', JSON.stringify(this.responseData) )
       //this.presentToast("success");
       //this.navCtrl.push(MyscoresPage);
       }, (err) =>
       {
         //Connection failed message
       });
     }else{
       alert("You must select data");
       loading.dismiss();
     }
    }
    else
    {
      //console.log(this.hh);
      if (this.update.email && this.update.id && this.update.sch && this.update.week && this.update.hourtime && this.update.minutetime && this.update.token && this.update.mobile)
      {
      console.log(this.update);
      this.authProvider.postData(this.update, "index.php?t=publish.dataconfigimam").then((result) =>{
      //loading.dismiss();
      this.responseData = result;
      console.log(JSON.stringify(this.responseData));
      //alert("update");
      loading.dismiss();
      this.navCtrl.setRoot(MynotesPage);
      //localStorage.setItem('update', JSON.stringify(this.responseData) )
      //this.presentToast("success");
      //this.navCtrl.push(MyscoresPage);
      }, (err) => {
        //Connection failed message
      });
     }
     else{
       alert("You must select data");
     }
     setTimeout(() => {
       loading.dismiss();
     }, 5000);
   }
}
}
