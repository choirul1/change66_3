import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoteconfigPage } from './noteconfig';

@NgModule({
  declarations: [
    NoteconfigPage,
  ],
  imports: [
    IonicPageModule.forChild(NoteconfigPage),
  ],
})
export class NoteconfigPageModule {}
