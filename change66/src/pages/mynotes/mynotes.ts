import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { NoteconfigPage } from '../noteconfig/noteconfig';
/**
 * Generated class for the MynotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mynotes',
  templateUrl: 'mynotes.html',
})
export class MynotesPage {
  myParam: string = '';
  items = [];
  responseData : any;
  userSearch = {"email":""};
  userName = {"FirstName":"", "LastName":""};
  constructor(public navCtrl: NavController,
              public authProvider: AuthProvider) {
              //public navParams: NavParams
  }
  ionViewDidLoad() {
    //console.log('ionViewDidLoad MynotesPage');
    let user = JSON.parse(localStorage.getItem('userData'));
    //console.log(this.user);
    this.userSearch.email = user.userData.Email;
    this.userName.FirstName = user.userData.FirstName;
    this.userName.LastName = user.userData.LastName;
    //alert(this.userName.LastName);
    this.authProvider.postData(this.userSearch, 'index.php?t=publish.data')
    .then((result) =>
    {
      this.responseData = result;

      this.items = this.responseData;
      console.log(this.items);

      //console.log(this.items);
    });
  }

  presentConfigNote(item) {
    this.navCtrl.push(NoteconfigPage, { 'myParam': item.id });
    //this.detail.schdet = schdet.schedule_id;
    console.log(item.id);
  }

  doRefresh(refresher) {
      console.log('Started', refresher);
      setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
      }, 4000);
  }
}
