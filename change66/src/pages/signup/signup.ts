import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { ModalcompanyPage } from '../modalcompany/modalcompany';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  myParam = '';
  catagories: any;
  resposeData : any;
  reg = {"firstname":"", "lastname":"", "email":"","password":"","authenRegistration":"1", "company":"", "categori":"", "companyid":"", "catid":""};
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,  private toastCtrl:ToastController, private authProvider: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  presentProfileModal() {
   let profileModal = this.modalCtrl.create(ModalpagePage);
   profileModal.onDidDismiss(data => {
     console.log(data);
     this.reg.categori = data.kategori;
     this.reg.catid = data.role;
   });
   profileModal.present();
 }

 presentCompanyModal() {
  let companyModal = this.modalCtrl.create(ModalcompanyPage);
  companyModal.onDidDismiss(data => {
    console.log(data);
    this.reg.company = data.CompanyName;
    this.reg.companyid = data.CompanyID;
  });
  companyModal.present();
}

// {firstname:reg.firstname, lastname:reg.lastname, email:reg.email, password:reg.password,authenRegistration:1,companyid:$scope.CompanyID,catid:$scope.CatID})
signup(){
  console.log(this.reg);
 if(this.reg.firstname && this.reg.lastname && this.reg.email && this.reg.password && this.reg.authenRegistration && this.reg.companyid && this.reg.catid )
 {
  this.authProvider.postData(this.reg, "cekperson.php").then((result) =>{
  this.resposeData = result;
  console.log(this.resposeData);
  localStorage.setItem('userData', JSON.stringify(this.resposeData) )
  this.presentToast("Register Success");
  this.navCtrl.push(LoginPage);

//   if(this.resposeData.userData){
//    localStorage.setItem('reg', JSON.stringify(this.resposeData) )
//   this.navCtrl.push(TabsPage);
// }
// else{
//   this.presentToast("Please give valid username and password");
// }



  }, (err) => {
    //Connection failed message
  });
 }
 else{
  this.presentToast("Error");
 }

}

presentToast(msg) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 2000
  });
  toast.present();
}

}
