import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

import { HomePage } from '../home/home';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  userData = {};
  responseData : any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              private authProvider: AuthProvider,
              private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login()
  {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    let alert = this.alertCtrl.create({
      title: 'Login Fail',
      subTitle: 'Wrong Username or Password',
      buttons: ['Dismiss']
    });
    //console.log(this.userData);
    loading.present();
    this.authProvider.postData(this.userData, 'cekperson.php')
    .then(
                (result) => {
                  loading.dismiss();
                  this.responseData = result;
                  //console.log(this.responseData);
                  if(this.responseData.PersonID == 0)
                  {
                    alert.present();
                  }
                  else
                  {
                    localStorage.setItem('userData', JSON.stringify(this.responseData));
                    this.navCtrl.setRoot(HomePage);
                  }
                  //console.log(this.responseData);
                }
              );
    //
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

}
