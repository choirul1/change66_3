import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyscoresPage } from './myscores';

@NgModule({
  declarations: [
    MyscoresPage,
  ],
  imports: [
    IonicPageModule.forChild(MyscoresPage),
  ],
})
export class MyscoresPageModule {}
