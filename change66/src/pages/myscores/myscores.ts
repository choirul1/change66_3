import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-myscores',
  templateUrl: 'myscores.html',
})
export class MyscoresPage {
  userName = {"FirstName":"", "LastName":""};
  userData = {"email":"", mobile:1};
  datasch = [];
  detail = {mobile:4,"schdet":""};
  resposeData : any;
  // dataSchDetail = [];
  responseData : any;
  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public authProvider: AuthProvider,
              public alertCtrl: AlertController,
              //private toastCtrl:ToastController,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad MyscoresPage');
    let user = JSON.parse(localStorage.getItem('userData'));
    //console.log(this.user);
    this.userData.email = user.userData.Email;
    this.userName.FirstName = user.userData.FirstName;
    this.userName.LastName = user.userData.LastName;
    //alert(this.userData.email);
    //console.log(this.userData);
    this.authProvider.postData(this.userData, 'index.php?t=publish.datagetperson')
    .then((result) =>
    {
      this.responseData = result;

      this.datasch = this.responseData.arrSch;
      // this.dataschall = this.responseData;
      console.log(this.datasch);

    });

  }

  showConfirm(schdet,Note){
    this.detail.schdet = schdet.schedule_id;
    //console.log(this.detail.schedule_id);
    let loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });
    loading.present();
    // alert("dhdg");
    let alert = this.alertCtrl.create({
          title: 'Hei, du har en oppgave du skal utføre !',
          message:  Note + '<br>jeg har gjennomført som avtalt',
          buttons: [
            {
              text: 'Disagree',
              handler: () => {
                console.log('Disagree clicked');
              }
            },
            {
              text: 'Agree',
              handler: () => {
                console.log('Agree clicked');
                if(this.detail.schdet && this.detail.mobile )

                {
                 console.log(this.detail);
                 this.authProvider.postData(this.detail, "index.php?t=publish.datagetpersonupdate").then((result) =>{
                 loading.dismiss();
                 this.resposeData = result;
                 console.log(this.resposeData);
                 this.ionViewDidLoad();
                 //localStorage.setItem('detail', JSON.stringify(this.resposeData) )
                 //this.presentToast("success");
                 //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                 }, (err) => {
                   //Connection failed message
                 });
                }
                else{
                 //this.presentToast("Error");
                }
                setTimeout(() => {
                  //loading.dismiss();
                }, 5000);
              }
            }
          ]
        });

        alert.present();



  }

  doRefresh(refresher) {
      console.log('Started', refresher);
      setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
      }, 4000);
  }
  // presentToast(msg) {
  //   let toast = this.toastCtrl.create({
  //     message: msg,
  //     duration: 2000
  //   });
  //   toast.present();
  // }


}
